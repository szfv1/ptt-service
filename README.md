# ptt-service

A service to handle push-to-talk on linux, by unmuting and muting all your microphones in pulseaudio.

This is useful in the following situations:

- You are using an application that doesn't support push-to-talk at all (e.g. Zoom, Slack)
- You are using a Wayland DE that doesn't support binding key release events (e.g. Gnome)
- You are using Wayland and there's no api to trigger push-to-talk in the application you are using (e.g. via dbus)

## Dependencies

- libudev
- libinput
- libpulseaudio

### Ubuntu

`sudo apt-get install libudev-dev libinput-dev libpulseaudio-dev`

## Usage

- Run as root, this is necessary so it can listen to your keypresses.
- Run with the following compulsory arguments:
    - `user`: The username of the user that pulseaudio was started by, usually the user you log in as
    - `xdg_runtime_dir`: The value of the XDG_RUNTIME_DIR env var for the user, e.g. `/run/user/1000`
    - `keycode`: A keycode of a key that you want to use for push-to-talk, e.g. `97` for Right Control. This argument can be supplied multiple times if you want to use multiple keys, and should also support mouse buttons.

e.g. `sudo ptt-service --user user --xdg-runtime-dir /run/user/1000 --keycode 97 --keycode 276`
