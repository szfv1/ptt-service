use clap::Parser;
use input::{
    event::{
        keyboard::{
            KeyState, {KeyboardEvent, KeyboardEventTrait},
        },
        pointer::{ButtonState, PointerEvent::*},
        Event::*,
    },
    Libinput, LibinputInterface,
};
//use libpulse_binding::context::{introspect::Introspector, Context};
use nix::{
    fcntl::{open, OFlag},
    sys::stat::Mode,
    unistd::close,
};
use std::os::unix::prelude::RawFd;
use std::{collections::HashSet, path::Path, process::Command, thread::sleep, time::Duration};

#[derive(Parser, Debug)]
#[clap(long_about = None)]
struct Args {
    #[clap(
        long,
        help = "A keycode of a key that you want to use for push-to-talk. This argument can be supplied multiple times if you want to use multiple keys, and should also support mouse buttons."
    )]
    keycode: Vec<u32>,
    #[clap(
        long,
        help = "The username of the user that pulseaudio was started by, usually the user you log in as"
    )]
    user: String,
    #[clap(
        long,
        help = "The value of the XDG_RUNTIME_DIR env var for the user supplied in the --user argument"
    )]
    xdg_runtime_dir: String,
    #[clap(long)]
    verbose: bool,
}

struct LibinputInterfaceRaw;

impl LibinputInterfaceRaw {
    fn seat(&self) -> String {
        String::from("seat0")
    }
}

impl LibinputInterface for LibinputInterfaceRaw {
    fn open_restricted(&mut self, path: &Path, flags: i32) -> std::result::Result<RawFd, i32> {
        if let Ok(fd) = open(path, OFlag::from_bits_truncate(flags), Mode::empty()) {
            Ok(fd)
        } else {
            Err(1)
        }
    }

    fn close_restricted(&mut self, fd: RawFd) {
        let _ = close(fd);
    }
}

fn set_mic_mute(args: &Args, state: bool) {
    //TODO: Use libpulseaudio directly rather than calling out to pacmd
    //introspector.get_source_info_list(|| {});
    //introspector.set_source_output_mute(1, false, None);
    let state_string = if state {
        "on".to_string()
    } else {
        "off".to_string()
    };
    if args.verbose {
        println!("mute: {}", state_string);
    }
    let pacmd_command = format!(
        r"pacmd list-sources | grep -oP 'index: \d+' | sed 's|index: ||g' | xargs -I{{}} pactl set-source-mute {{}} {}",
        state_string
    );
    let output = Command::new("sh")
        .args([
            "-c",
            &format!(
                "sudo -u {} env XDG_RUNTIME_DIR={} sh -c \"{}\"",
                args.user, args.xdg_runtime_dir, pacmd_command
            ),
        ])
        .output()
        .expect("Failed to set mute state");
    if args.verbose {
        println!("{:?}", output);
    }
}

fn handle_input_events(args: Args) {
    let keycodes: HashSet<u32> = HashSet::from_iter(args.keycode.clone().into_iter());
    let mut libinput_context = Libinput::new_with_udev(LibinputInterfaceRaw);
    libinput_context
        .udev_assign_seat(&LibinputInterfaceRaw.seat())
        .unwrap();

    loop {
        libinput_context.dispatch().unwrap();
        //let main_loop = libpulse_binding::mainloop::standard::Mainloop::new()
        //    .expect("Failed to create main loop");
        //let pulse_context =
        //    Context::new(&main_loop, "ptt-server").expect("Failed to connect to pulse");
        //let mut introspector = pulse_context.introspect();

        for event in libinput_context.by_ref() {
            match event {
                Keyboard(KeyboardEvent::Key(keyboard_key_event)) => {
                    if keycodes.contains(&keyboard_key_event.key()) {
                        let key_state = keyboard_key_event.key_state();
                        match key_state {
                            KeyState::Pressed => set_mic_mute(&args, false),
                            KeyState::Released => set_mic_mute(&args, true),
                        };
                    };
                }
                Pointer(Button(button_event)) => {
                    if keycodes.contains(&button_event.button()) {
                        let button_state = button_event.button_state();
                        match button_state {
                            ButtonState::Pressed => set_mic_mute(&args, false),
                            ButtonState::Released => set_mic_mute(&args, true),
                        };
                    };
                }
                _ => {}
            }
        }

        sleep(Duration::from_millis(10));
    }
}

fn main() {
    let args = Args::parse();
    handle_input_events(args);
}
